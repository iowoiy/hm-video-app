import 'package:flutter/material.dart';
import 'package:game/screens/lobby.dart';

class LayoutGameScreen extends StatelessWidget {
  const LayoutGameScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameLobby();
  }
}
