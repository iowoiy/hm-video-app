enum ControlsOverlayType { none, progress, playPause, middleTime }

enum SideControlsType { none, sound, brightness }

enum ChargeType {
  none,
  free, // 1: 免費
  coin, // 2: 金幣
  vip, // 3: VIP
}
