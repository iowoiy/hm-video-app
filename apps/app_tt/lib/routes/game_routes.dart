library game_routes;

import 'package:game/enums/game_app_routes.dart';
import 'package:shared/navigator/delegate.dart';

final Map<String, RouteWidgetBuilder> gameRoutes = {};
