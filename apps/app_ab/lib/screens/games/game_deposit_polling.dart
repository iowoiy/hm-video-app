// paymentPage:2 列表
import 'package:flutter/material.dart';
import 'package:game/screens/game_deposit_polling_screen/index.dart';

class GameDepositPollingScreen extends StatelessWidget {
  const GameDepositPollingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameDepositPolling();
  }
}
