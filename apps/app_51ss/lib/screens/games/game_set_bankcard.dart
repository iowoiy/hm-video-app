import 'package:flutter/material.dart';
import 'package:game/screens/game_set_bankcard_screen/index.dart';

class GameSetBankCardScreen extends StatelessWidget {
  const GameSetBankCardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameSetBankCard();
  }
}
