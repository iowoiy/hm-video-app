import 'package:flutter/material.dart';
import 'package:game/screens/game_withdraw_screen/index.dart';

class GameWithdrawScreen extends StatelessWidget {
  const GameWithdrawScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameWithdraw();
  }
}
