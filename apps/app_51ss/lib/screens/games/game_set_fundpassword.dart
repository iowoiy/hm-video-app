import 'package:flutter/material.dart';
import 'package:game/screens/game_set_fundpassword_screen/index.dart';

class GameSetFundPasswordScreen extends StatelessWidget {
  const GameSetFundPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameSetFundPassword();
  }
}
