// paymentPage:2 列表
import 'package:flutter/material.dart';
import 'package:game/screens/game_deposit_list_screen/index.dart';

class GameDepositListScreen extends StatelessWidget {
  const GameDepositListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameDepositList();
  }
}
