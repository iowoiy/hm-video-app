// paymentPage:2 列表
import 'package:flutter/material.dart';
import 'package:game/screens/game_withdraw_record_screen/index.dart';

class GameWithdrawRecordScreen extends StatelessWidget {
  const GameWithdrawRecordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameWithdrawRecord();
  }
}
