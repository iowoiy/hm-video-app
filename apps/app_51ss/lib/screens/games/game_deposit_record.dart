// paymentPage:2 列表
import 'package:flutter/material.dart';
import 'package:game/screens/game_deposit_record_screen/index.dart';

class GameDepositRecordScreen extends StatelessWidget {
  const GameDepositRecordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GameDepositRecord();
  }
}
