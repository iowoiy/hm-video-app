import 'package:flutter/material.dart';
import 'package:game/screens/game_payment_result_screen/index.dart';

class GamePaymentResultScreen extends StatelessWidget {
  const GamePaymentResultScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const GamePaymentResult();
  }
}
